#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char* arithmatoy_add(int base, const char* lhs, const char* rhs) {
 if (VERBOSE) {
 fprintf(stderr, "add: entering function\n");
 }
  int lhs_len = strlen(lhs);
  int rhs_len = strlen(rhs);
  int max_len = (lhs_len > rhs_len) ? lhs_len : rhs_len;

  // Allouer suffisamment d'espace pour contenir le résultat de l'addition.
  char* result = calloc(max_len + 2, sizeof(char));
  result[max_len + 1] = '\0';

  // Initialiser les variables nécessaires à l'addition.
  int carry = 0;
  int lhs_idx = lhs_len - 1;
  int rhs_idx = rhs_len - 1;
  int result_idx = max_len;

  // Parcourir les chaînes de caractères de droite à gauche, en additionnant
  // les chiffres correspondants et en gérant la retenue.
  while (lhs_idx >= 0 || rhs_idx >= 0) {
    int lhs_digit = (lhs_idx >= 0) ? lhs[lhs_idx] - '0' : 0;
    int rhs_digit = (rhs_idx >= 0) ? rhs[rhs_idx] - '0' : 0;
    int sum = lhs_digit + rhs_digit + carry;

    carry = sum / base;
    int digit = sum % base;

    result[result_idx] = digit + '0';

    --lhs_idx;
    --rhs_idx;
    --result_idx;
  }

  // Si une retenue est restée après l'addition des deux nombres, l'ajouter
  // au résultat final.
  if (carry != 0) {
    result[result_idx] = carry + '0';
  }

  // Supprimer les zéros non significatifs en début de chaîne.
  while (*result == '0' && *(result + 1) != '\0') {
    ++result;
  }

  return result;
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }
  // Fill the function, the goal is to compute lhs - rhs (assuming lhs > rhs)
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result
  size_t lhs_len = strlen(lhs);
  size_t rhs_len = strlen(rhs);
  size_t max_len = lhs_len > rhs_len ? lhs_len : rhs_len;
  char *result = calloc(max_len + 2, sizeof(char));
  int carry = 0;
  for (int i = 0; i < max_len; ++i) {
    int lhs_val = i < lhs_len ? get_digit_value(lhs[lhs_len - i - 1]) : 0;
    int rhs_val = i < rhs_len ? get_digit_value(rhs[rhs_len - i - 1]) : 0;
    int diff = lhs_val - rhs_val - carry;
    if (diff < 0) {
      diff += base;
      carry = 1;
    } else {
      carry = 0;
    }
    result[i] = to_digit(diff);
  }
  return reverse(drop_leading_zeros(result));
}


  


char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  size_t lhs_len = strlen(lhs);
  size_t rhs_len = strlen(rhs);
  char *result = calloc(lhs_len + rhs_len + 1, sizeof(char));
  for (int i = 0; i < lhs_len; ++i) {
    int carry = 0;
    for (int j = 0; j < rhs_len || carry; ++j) {
      int lhs_val = get_digit_value(lhs[lhs_len - i - 1]);
      int rhs_val = j < rhs_len ? get_digit_value(rhs[rhs_len - j - 1]) : 0;
      int prod = lhs_val * rhs_val + carry + get_digit_value(result[i + j]);
      carry = prod / base;
      prod %= base;
      result[i + j] = to_digit(prod);
    }
  }
  return reverse(drop_leading_zeros(result));
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
