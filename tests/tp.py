# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    return "0" if n == 0 else "S" + nombre_entier(n - 1)

def S(n: str) -> str:
    return "S" + n

def addition(a: str, b: str) -> str:
    if a == "0":
        return b
    elif b == "0":
        return a
    else:
        return S(addition(a[:-1], b[:-1]))

def multiplication(a: str, b: str) -> str:
    if a == "0" or b == "0":
        return "0"
    else:
        return addition(b, multiplication(a[:-1], b))

# factorielle (itérative)
def facto_ite(n: int) -> int:
    result = 1
    for i in range(1, n+1):
        result *= i
    return result

# factorielle (récursive)
def facto_rec(n: int) -> int:
    if n == 0:
        return 1
    else:
        return n * facto_rec(n-1)


# fibonacci (récursive)
def fibo_rec(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibo_rec(n-1) + fibo_rec(n-2)

# fibonacci (itérative)
def fibo_ite(n: int) -> int:
    a, b = 0, 1
    for _ in range(n):
        a, b = b, a+b
    return a

# Nombre d'or
def golden_phi(n: int) -> float:
    if n == 0:
        return 1
    else:
        return 1 + 1 / golden_phi(n-1)

# Racine carrée de 5
def sqrt5(n: int) -> float:
    if n == 0:
        return 2
    else:
        return 2 + 1 / sqrt5(n-1)

# Puissance (récursive)
def pow(base: float, exp: int) -> float:
    if exp == 0:
        return 1
    elif exp % 2 == 0:
        return pow(base**2, exp//2)
    else:
        return base * pow(base, exp-1)
