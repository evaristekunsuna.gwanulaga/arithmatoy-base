#!/bin/bash

# Create virtual environment
#python3 -m venv .venv

# Activate virtual environment
#if [[ "$OSTYPE" == "linux-gnu"* || "$OSTYPE" == "darwin"* ]]; then
#    source .venv/bin/activate
#elif [[ "$OSTYPE" == "win32" ]]; then
#    source .venv/scripts/activate
#fi

# Install pytest
#python3 -m pip install pytest

# Run TP tests
#python3 -m pytest ../tests/test_tp.py
python3 -m pytest ../tests/test_tp.py -k test_nombre_entier
python3 -m pytest ../tests/test_tp.py -k test_successeur
python3 -m pytest ../tests/test_tp.py -k test_addition
python3 -m pytest ../tests/test_tp.py -k test_multiplication
python3 -m pytest ../tests/test_tp.py -k test_facto_ite
python3 -m pytest ../tests/test_tp.py -k test_facto_rec
python3 -m pytest ../tests/test_tp.py -k test_fibo_rec
python3 -m pytest ../tests/test_tp.py -k test_fibo_ite




# Deactivate virtual environment
#deactivate
